<?php
#Name:Create Files v1
#Description:Create files by incrementing from one Ritchey Base Number to another. Corresponding files are saved. Returns "TRUE" success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values. The script has external software dependencies (FFmpeg, Imagemagick).
#Arguments:'start_number' (required) is a string containing the smaller Ritchey Base Number to increment from. 'end_number' (required) is a string containing a larger Ritchey Base Number to increment to. 'destination' is a path to save files to. 'file_type' (optional) specifies which file types to keep. 'moderation' (optional) specifies if the script should pause after saving a file so it can be reviewed by the user. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):start_number:string:required,end_number:string:required,destination:path:required,file_type:string:optional,moderation:bool:optional,display_errors:bool:optional
#Content:
if (function_exists('create_files_v1') === FALSE){
function create_files_v1($start_number, $end_number, $destination, $file_type = NULL, $moderation = NULL, $display_errors = NULL){
	$errors = array();
	$progress = '';
	##External Software Binaries
	if (@is_file('/usr/bin/ffmpeg') === FALSE){
		$errors[] = "FFmpeg";
	}
	if (@is_file('/usr/bin/convert') === FALSE){
		$errors[] = "Imagemagick";
	}
	##Arguments
	if (@ctype_digit($start_number) === FALSE){
		$errors[] = "start_number";
	}
	if (@ctype_digit($end_number) === FALSE){
		$errors[] = "end_number";
	}
	if (@is_dir($destination) === FALSE){
		$errors[] = "destination";	
	}
	if ($file_type === 'video'){
		#Do nothing
	} else if ($file_type === 'photo'){
		#Do nothing
	} else if ($file_type === 'text'){
		#Do nothing
	} else if ($file_type === NULL){
		#Do nothing
	} else {
		$errors[] = "file_type";
	}
	if ($moderation === NULL){
		$moderation = FALSE;	
	}
	if ($moderation === TRUE OR $moderation === FALSE){
		#Do Nothing
	} else {
		$errors[] = "moderation";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;	
	}
	if ($display_errors === TRUE OR $display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task [Increment from $start_number to $end_number. For each number, if it is a valid Ritchey Base Number, if it is a requested file type, decode it to a file, and if moderation is TRUE pause before continuing.]
	if (@empty($errors) === TRUE){
		$location = realpath(dirname(__FILE__));
		require_once $location . '/dependencies/increment_large_number_v1.php';
		require_once $location . '/dependencies/random_number_v2.php';
		require_once $location . '/dependencies/ritchey_base_number_to_file_v2.php';
		###Create a loop for performing the operations on each increment
		do {
			###Ensure the number is even length, because Ritchey Base Numbers are always even length! If yes, perform file type checks, and decode to a file.
			if (@strpbrk(strlen($start_number), 13579) == TRUE){
				#Do nothing
			} else {
				$random = random_number_v2('10', FALSE);
				do {
					$random = random_number_v2('10', FALSE);
				} while (@is_file("{$destination}/delete_me_{$random}") === TRUE);
				###Decode to a file, but some numbers will be invalid Ritchey Base Numbers so check if file exists.
				ritchey_base_number_to_file_v2($start_number, "{$destination}/delete_me_{$random}", FALSE);
				if (@is_file("{$destination}/delete_me_{$random}") === TRUE){
					###If a file type check needs to be done, do so, and if necessary delete or keep the file
					if ($file_type === NULL){
						#Do nothing
					} else if ($file_type === 'text'){
						$string = @file_get_contents("{$destination}/delete_me_{$random}");
						if (@ctype_print($string) !== TRUE){
						#if (@ctype_graph($string) !== TRUE){
							$string = NULL;
							unlink("{$destination}/delete_me_{$random}");
						}
					} else if ($file_type === 'photo'){
						###Attempt to convert image using Imagemagick
						$random2 = random_number_v2('10', FALSE);
						do {
							$random2 = random_number_v2('10', FALSE);
						} while (@is_file("{$destination}/delete_me_{$random2}") === TRUE);
						@exec("convert \"{$destination}/delete_me_{$random}\" \"{$destination}/delete_me_{$random2}.png\"");
						if (@is_file("{$destination}/delete_me_{$random2}.png") === TRUE){
							unlink("{$destination}/delete_me_{$random2}.png");
						} else {
							unlink("{$destination}/delete_me_{$random}");
						}
					} else if ($file_type === 'video'){
						###Attempt to convert video using ffmpeg
						$random2 = random_number_v2('10', FALSE);
						do {
							$random2 = random_number_v2('10', FALSE);
						} while (@is_file("{$destination}/delete_me_{$random2}") === TRUE);
						@exec("ffmpeg -i \"{$destination}/delete_me_{$random}\" -vcodec libx264 -acodec aac \"{$destination}/delete_me_{$random2}.mp4\"");
						if (@is_file("{$destination}/delete_me_{$random2}.mp4") === TRUE){
							unlink("{$destination}/delete_me_{$random2}.mp4");
						} else {
							unlink("{$destination}/delete_me_{$random}");
						}
					}
					###Moderation
					if ($moderation === TRUE){
						if (@is_file("{$destination}/delete_me_{$random}") === TRUE){
							echo "'{$destination}/delete_me_{$random}' is ready for review. After, press enter to continue...\n";
							$user_input = fgets(STDIN);
							#$user_input = rtrim($user_input, "\n");
						}
					}
					###Rename file to it's SHA-256
					if (@is_file("{$destination}/delete_me_{$random}") === TRUE){
						$sha256 = @hash_file('sha256', "{$destination}/delete_me_{$random}");
						@rename("{$destination}/delete_me_{$random}", "{$destination}/{$sha256}");
					}
				}
			}
			$start_number = increment_large_number_v1($start_number, FALSE);
		} while ($start_number != $end_number);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE and @empty($errors === FALSE)){
		$message = @implode(", ", $errors);
		if (function_exists('create_files_v1_format_error') === FALSE){
			function create_files_v1_format_error($errno, $errstr){
				echo $errstr;
			}
		}
		set_error_handler("create_files_v1_format_error");
		trigger_error($message, E_USER_ERROR);
	}
	##Return
	if (@empty($errors) === TRUE){
		return TRUE;
	} else {
		return FALSE;
	}
}
}
?>